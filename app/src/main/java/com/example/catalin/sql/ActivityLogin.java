package com.example.catalin.sql;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ActivityLogin extends AppCompatActivity {

    //Objetos gráficos
    EditText txtUserId, txtUserPass;
    Button btnLogin;
    ProgressBar pbbar;

    //Connection SQL
    ConnectionSQL connectionSQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        //Inicialização do Connector e aquesição dos objetos gráficos

        connectionSQL = new ConnectionSQL();                            //Connector Inicializado
        txtUserId = (EditText) findViewById(R.id.txtUserId);            //Adquire o controlo de txtUserId
        txtUserPass = (EditText) findViewById(R.id.txtUserPass);        //Adquire o controlo de txtUserPass
        btnLogin = (Button) findViewById(R.id.btnLogin);                //Adquire o controlo de botão
        pbbar = (ProgressBar) findViewById(R.id.pbbar);                 //Adquire o controlo da progress bar
        pbbar.setVisibility(View.GONE);                                 //Declara-a invisível

        btnLogin.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Login login = new Login();
                Log.i("Connection", "Login");
                login.execute();
            }
        });

    }
    public class Login extends AsyncTask<String,String,String>{

        String msg = "";                                                      // Mensagem para o utilizador
        Boolean sucesso = false;                                               //Contorolo
        String userId = txtUserId.getText().toString();
        String passowrd = txtUserPass.getText().toString();

        //Metodos override : obrigatório implementar nesta classe


        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);
        }
        @Override
        protected String doInBackground(String... params) {
            if(userId.trim().equals("") || passowrd.trim().equals("")){
                msg = "Introduza credenciais para o Login";
            }
            else{
                try{
                    Connection con = connectionSQL.connect();
                    if(con == null){
                        msg = "Erro de ligação SQL Server :";
                    }
                    else{
                        String query = "Select * from Aluno where Username='" + userId + "' and Password ='" + passowrd + "'";

                        Statement stmt = con.createStatement();             //executa a dml
                        ResultSet rs = stmt.executeQuery(query);            //recebe o resultado
                        if (rs.next()){
                            msg = "Login bem sucedido";
                            sucesso = true;
                        }
                        else{
                            msg = "Credenciais inválidas";
                            sucesso = false;
                        }
                    }



                }
                catch(Exception ex){
                    sucesso = false;
                    msg = "Exeção:" + ex.getMessage();
                }
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            pbbar.setVisibility(View.GONE);
            Toast.makeText(ActivityLogin.this, s, Toast.LENGTH_SHORT).show();

            if(sucesso){
                //Se ligação ok, chama a próxima activity para tratar dados: iniserir , listar
                //startActivity(new Intent(ActivityLogin,this , proximaActivity,class));
                finish();   //Método desta classe para terminar a ação em background
            }
        }
    }

}
